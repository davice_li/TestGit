import unittest,os
from ddt import ddt,data,file_data,unpack

@ddt
class TestWork2(unittest.TestCase):
#读取字典数据
# '''
# {'name': 'lili', 'age': '16'}
#   {'sex': 'female', 'job': 'nurser'}
# ※以上运行的结果数据，就可以用来作为requests的请求参数~！※
# '''
    @data({'name':'lili','age':'16'}, {'sex':'female','job': 'nurser'})
    def test_01(self,a):
        print(a)
        pass

    #lili 16
    #female nurser
    #重点来了：首先结果展示的数据是字典里的value，没有打印key的值；其次@data里的数据key值和def方法里的形参
    #名称一定要一致，否则，打印的时候，就会报莫名的参数错误，这里就不做展示，爱学习的同学可以尝试一下~！
    @data({'name': 'lili', 'age': '16'}, {'sex': 'female', 'job': 'nurser'})
    @unpack
    def test_02(self,name,age):
        print(name,age)
        pass

if __name__ == '__main__':
    unittest.main