
import  unittest,os
from ddt import data,unpack,file_data,ddt

#读取元数据

'''
一般进行接口测试时，每个接口的传参都不止一种情况，一般会考虑正向、逆向等多种组合。所以在测试一个接口时通常会编写多条case，而这些case除了传参不同外，其实并没什么区别。
这个时候就可以利用ddt来管理测试数据，提高代码复用率。
※但要注意：正向和逆向的要分开写※
安装：pip install ddt
四种模式：第一步引入的装饰器@ddt；导入数据的@data；拆分数据的@unpack；导入外部数据的@file_data

作者：小二哥很二
链接：https://www.jianshu.com/p/78998bcf3e05
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
'''

@ddt
class TestWork1(unittest.TestCase):

    # '''NO.1单组元素'''
    @data(1,2,3)
    def test_01(self,value):
        print(value)
        pass

    '''NO.2多组未分解元素'''
    @data((1,2,3),(3,4,5))
    def test_02(self,value):
        print(value)
        pass

    '''NO.3多组分解元素'''
    @data((1,2,3),(3,4,5))
    @unpack
    def test_03(self,value1,value2,value3):   # 每组数据有3个值，所以设置3个形参
        print(value1,end=" ")
        print(value2, end=" ")
        print(value3)



if __name__ == '__main__':
    unittest.main