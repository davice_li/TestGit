import  unittest,os
from ddt import data,unpack,file_data,ddt

@ddt
class  TestWork(unittest.TestCase):
    #上面结果可以看出：无法运用到requests数据请求中，所以不是很实用
    #[{'name': 'lili', 'age': 12}, {'sex': 'male', 'job': 'teacher'}]
    @data([{'name':'lili','age':12},{'sex':'male','job':'teacher'}])
    def test_04(self,value):
        print(value)

    # ''' 读取列表数据 '''
    #{'name': 'lili', 'age': 12} {'sex': 'male', 'job': 'teacher'}
    #拆分后的运行结果，不带有[ ]，拆分是将列表中的2个字典拆分，所以有2个数据
    @data([{'name': 'lili', 'age': 12}, {'sex': 'male', 'job': 'teacher'}])
    @unpack
    def test_05(self, a,b):
        print(a,b)
        pass

if __name__ == '__main__':
    unittest.main()