#coding:utf-8


#
def loginSuc(driver,username,passwd,yzm):
    try:
        l1=driver.find_element_by_xpath('//input[@placeholder="账号"]')
        l1.clear()
        l1.send_keys(username)
        l2=driver.find_element_by_xpath('//input[@placeholder="密码"]')
        l2.clear()
        l2.send_keys(passwd)
        driver.find_element_by_xpath('//input[@placeholder="验证码"]').send_keys(yzm)

        driver.find_element_by_xpath('//button').click()
    except  Exception as e :
        print(e)
    pass

def loginFail(driver,username,passwd,yzm):
    l1=driver.find_element_by_xpath('//input[@placeholder="账号"]')
    l1.clear()
    l1.send_keys(username)
    l2=driver.find_element_by_xpath('//input[@placeholder="密码"]')
    l2.clear()
    l2.send_keys(passwd)
    driver.find_element_by_xpath('//input[@placeholder="验证码"]').send_keys(yzm)

    driver.find_element_by_xpath('//button').click()

    txt= driver.find_element_by_xpath("//div[@class='ivu-message-custom-content ivu-message-error']/span/text()")
    pass
    return txt
