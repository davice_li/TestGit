#coding:utf-8
from appium import webdriver
from time import sleep

#appium 环境配置
# 如何查找包名和Activity
#adb devices 查看设备信息
#adb shell dumpsys activity  activities >abc.txt
#打开abc.txt文件，查找realActivity 就可以看到包名和主activity
desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '7.1.2'
desired_caps['deviceName'] = '127.0.0.1:21503 device'
desired_caps['appPackage'] = 'com.ss.android.article.news'
desired_caps['appActivity'] = 'com.ss.android.article.news.activity.MainActivity'
desired_caps['noReset'] = 'true'
#desired_caps['app']='C:\\Users\\Administrator\\base.apk'

driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)


driver.find_element_by_xpath('//android.view.View[@content-desc="视频"]').click()

sleep(2)
size = driver.get_window_size()
print(size)
#屏幕的宽度 width
x=size['width']
#屏幕的高度 height
y=size['height']
def  swipe_right(num):
    """
    向右滑动
    """
    x1=x*0.9
    y1=y*0.5
    x2=x*0.1
    t=1000
    n=num#n表示滑动次数
    sleep(1)
    for i in range(n):
        driver.swipe(x1,y1,x2,y1,t)

def swipe_left():
    """
    向右滑动
    """
    x1=x*0.1
    y1=y*0.5
    x2=x*0.9
    t=1000
    n=2#n表示滑动次数
    sleep(1)
    for i in range(n):
        driver.swipe(x1,y1,x2,y1,t)

def swipe_down():
    """
    向下滑动
    """
    x1=x*0.5
    y1=y*0.1
    y2=y*0.9
    t=1000
    n=3#n表示滑动次数
    sleep(1)
    for i in range(n):
        driver.swipe(x1,y1,x1,y2,t)
def swipe_up():
    """
    向上滑动
    """
    x1=x*0.5
    y1=y*0.9
    y2=x*0.1
    t=10000
    n=1#n表示滑动次数
    sleep(1)
    for i in range(n):
        driver.swipe(x1,y1,x1,y2,t)


swipe_right(2)
driver.tap([(125,347)],5)
driver.tap([(125,347)],5)
driver.tap([(125,347)],5)
while True:
    sleep(5)
    swipe_up()
driver.close()