#coding:utf-8
from appium import webdriver
from time import sleep

#appium 环境配置
# 如何查找包名和Activity
#adb devices 查看设备信息
#adb shell dumpsys activity  activities >abc.txt
#打开abc.txt文件，查找realActivity 就可以看到包名和主activity
desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '7.1.2'
desired_caps['deviceName'] = '127.0.0.1:21503 device'
desired_caps['appPackage']  = 'com.ibox.calculators'
desired_caps['appActivity'] = 'com.ibox.calculators.CalculatorActivity'
desired_caps['noReset'] = 'true'
#desired_caps['app']='C:\\Users\\Administrator\\base.apk'

driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

driver.find_element_by_id("com.ibox.calculators:id/digit7").click()
driver.find_element_by_id("com.ibox.calculators:id/minus").click()
driver.find_element_by_id("com.ibox.calculators:id/digit4").click()
driver.find_element_by_id("com.ibox.calculators:id/equal").click()

driver.close()
