#coding:utf-8

# tup1 = ('Google', 'Runoob', 1997, 2000)
# tup2 = (1, 2, 3, 4, 5 )
# tup3 = "a", "b", "c", "d","d"
# print(type(tup3))
# # tup3[0] =2    #错误的写法  元素不可以修改
# print(tup3[-1::-1])
# print(tup3.count("d"))
# tup = tup1+tup2+tup3   #可以创建一个新的元组
# print(tup)
#
# for i in tup:
#     print(i)
# tup =tup*2
# # del(tup)
# print(tup)



###############内置函数
#len(tup)  元组的长度
#max(tup)  元组中最大的
#min(tup)  元组中最小的
#tuple(list)   将可迭代系列转换为元组。


lis = ['Google', 'Runoob', 1997, 2000]
#列表转换成元组
tup = tuple(lis)
print(type(tup))
print(tup)
#元组转换成列表
list1 =list(tup)
print(list1)