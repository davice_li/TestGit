#coding:utf-8
# -*- coding:utf-8 -*-
import math
import random
# if True:
#     print("\a")
#     print('a\tb')
# else:
#     print()

# str1 = '123456789'
# print(str1[0:-1])
#print(str1[-9:])
# print(str1[0])

# print(len(str1))

#换行和不换行
# print("x",end="")
# print("y")

# a, b, c, d = 20, 5.5, True, 4+3j

# print(type(a), type(b), type(c), type(d))


# print(isinstance(b,float))

# str1 = '123456789'
#
# print(str1[9::-1])

# print(math.ceil(4.01))
# print(math.floor(4.9))

# cai = ["<。)#)))≦","麻婆豆腐","红烧腐竹","红烧肉","小龙虾"]
#
# index = random.choice(range(5))
# print(index)
# print(cai[index])

# print(random.random())


# print("{}*{}={}".format(1,1,1))
# print("hello,%s"%("lily"))

# print("lilyr".endswith("ly"))
# print("lilyr".startswith("li"))

#定义一个列表
# list1 = ["wallace","wall","liy","eric","bob","saliy","david"]
# print(list1)  #打印列表
# print(len(list1))   #列表的长度
# print(range(len(list1)))   #表示0~N 范围

# for i in range(len(list1)):      #计算了列表的长度，range范围
#     if list1[i].startswith("wa"):    #i 是列表的索引
#         print(list1[i])    #把列表索引代表的值打出来

# for s in list1:    #取的列表里的值
#     if s.startswith("wa"):
#         print(s)

# sums=0
# for number in range(10):
#     if number%2 ==1:
#         print(number)
#         sums = sums+number
# print(sums)

# a =1
# while a<=10:
#     print(a)
#     a=a+1

# for item in range(1,11):
#     print(item)

# 1~10   不打印7

# for  item in range(1,11):
#     if item !=7:
#         print(item)

# for  item in range(1,11):
#     if item ==7:
#         break
#     print(item)

# for  item in range(1,11):
#     if item ==7:
#         continue
#     print(item)

s = "张三"
# print(s.encode("utf-8"))
# print(s.encode("GBK"))

# print(s.encode("unicode-escape"))

# print(s.join("abc"))

