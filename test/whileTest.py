#coding= utf-8

# a =1
# while a<10:
#     print(a)
#     a+=2

numbers = [12,37,41,56,88,97,3]
# print(len(numbers))
# print(numbers.pop())
# print(len(numbers))
# print(numbers.pop())
# print(len(numbers))

# even = []
# odd = []
# while len(numbers)>0:
#     number = numbers.pop()
#     if(number%2) ==0:
#         even.append(number)
#     else:
#         odd.append(number)


# count =0
# while count<9:
#     print("你好！"+str(count))
#     count=count+1
# print("再见")

# i = 1
# while i < 10:
#     i += 1
#     if i%2 > 0:     # 非双数时跳过输出
#         continue
#     print(i)

# i = 1
# while 1:            # 循环条件为1必定成立
#     print(i)        # 输出1~10
#     i += 1
#     if i > 10:     # 当i大于10时跳出循环
#         break

# while 1:
#     print("1")

# count = 0
# while count < 5:
#    print(count, " is  less than 5")
#    count = count + 1
# else:
#    print(count, " is not less than 5")