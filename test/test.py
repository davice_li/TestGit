#coding=utf-8
#print("hello!")
#赋值 给一个字符赋值
#数字
num = 1
#字符类型,双引号括起来的
zf = "abcdfeg"
#单引号括起来的
zf1 = 'abc'
f = 1.12

'''
num =num +1   #2
num = num +1  #3
print(num)
f =num +f
print(f)
print(num<f)   #true   布尔有两值  false  true

zf = zf+zf
print(zf)
print(zf+zf1+"lily")
'''
#连续赋值
#a = b = c = 1
#print("a= "+str(a)+" b= "+str(b)+" c= "+str(c))
#分开赋值
# a, b, c = 1, 2, "john"
# print("a= "+str(a)+" b= "+str(b)+" c= "+c)

str1 = "abcdef"
#print(str1[1:5])  #从第二个元素
#print(str1[0:])   #取出所有的值
# print(str1[-6:])
#print(str1*2)
# letters ='checkio'
# print(letters[1:6:2])  #结果hci

#列表实验
list = [ 'runoob', 786 , 2.23, 'john', 70.2 ]

# print(list[0:])
# print(list[1:3])
# print(list[1:5])
#print(list[-1:])  #前面开始位置
# print(list[-1:6])  #后面的数代表取到那个位置
# list[1] ="abc"
# print(list)

#元组是不允许更新的，它是一个只读的。而列表是允许更新的
# tuple = ( 'runoob', 786 , 2.23, 'john', 70.2 )
# tinytuple = (123, 'john')
# tuple[1] ="abc"
# print(tuple)

#字典
dict = {}
dict['one'] = "This is one"
dict[2] = "This is two"
#
# tinydict = {'name': 'runoob','code':6734, 'dept': 'sales'}
#
# print(tinydict["name"])
# tinydict["name"] ="eric"
# print(tinydict["name"])
# tinydict.keys()
# print(tinydict.keys())
# tinydict.values()
# print(tinydict.values())
# l1 = tinydict.keys()
# l2 = tinydict.values()
#
# print(l1)

# print("hello,%s"%("lily"))
#
# print(" HelLo lily bye".istitle())

print(range(8))
print(len(" gougouQ"))