#coding:utf-8

# def adds(x,y):
#     return x+y
#
# def cf(x,y):
#     return x*y
#
# print(adds(10,10))
# print(cf(10,10))

# def  bj(x,y):
#     if x>y:
#         return x
#     else:
#         return y
#
# print(bj(9,15))

# def substr(str1,i,j):
#     return str1[i:j+1]
#
# print(substr("abcdefg",1,4))
#可变长参数
# def printinfo( arg1, *vartuple ):
#    "打印任何传入的参数"
#    print ("输出: ")
#    print (arg1)
#    print (vartuple)
#    for i in vartuple:
#       print(i)
#
# printinfo(70,69,10,11,8,10)

# 可写函数说明
# def printinfo( arg1, **vardict ):
#    "打印任何传入的参数"
#    print ("输出: ")
#    print (arg1)
#    print (vardict)
#    print(vardict.keys())
#    print(vardict.values())
#
# # 调用printinfo 函数
# printinfo(1, a=2,b=3)

def  sum(arg1,arg2):
   return arg1+arg2
#
# # 可写函数说明
# sum = lambda arg1, arg2: arg1 + arg2
#
# # 调用sum函数
# print ("相加后的值为 : ", sum( 10, 20 ))
# print ("相加后的值为 : ", sum( 20, 20 ))