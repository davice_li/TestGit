#coding=utf-8
import os,sys
from selenium import webdriver


# for letter in 'Python':     # 第一个实例
#    print("当前字母: %s" % letter)

# fruits = ['banana', 'apple',  'mango']
# for fruit in fruits:        # 第二个实例
#    print ('当前水果: %s'% fruit)

# print(range(len(fruits)))
# print(fruits[3])
# fruits = ['banana', 'apple',  'mango']
# for index in range(len(fruits)):
#    print ('当前水果 : %s' % fruits[index])

#九九乘法表
# for i in range(1,10):
#     for j in range(1,i+1):
#         # print("i="+str(i) +" j = "+str(j))
#         print('{}x{}={}\t'.format(j, i, i*j), end=' ')
#     print()


# from random import shuffle
#
# lst = ['banana', 'apple',  'mango']
# shuffle(lst)
# print(lst)
